<h1>Secretaría Ejecutiva del Sistema Estatal Anticorrupción</h1>
<h2>Dirección General de Servicios Tecnológicos y Plataforma Digital</h2>
<h3>API-SPIC</h3>
<h1>Tecnologías</h1>
<ul>
<li>Java Development Kit 1.8.172</li>
<li>Apache Tomcat 9.0.19</li>
<li>MongoDB Enterprise 4.2.14</li>
<li>Apache Maven 3.8.1</li>
<h1>Manuales</h1>
<a href="https://gitlab.com/sesaemm_pde/apispic/-/blob/master/Documentacion/M_I_API_S2_V1.pdf">Manual de instalación</a>
<br>
<a href="https://gitlab.com/sesaemm_pde/apispic/-/blob/master/Documentacion/BD_API_S2_V1.pdf">Manual de BD</a>
<br>
<a href="https://gitlab.com/sesaemm_pde/apispic/-/blob/master/Documentacion/M_U_API_S2_V1.pdf">Manual de usuario</a>
<br>
</ul>
