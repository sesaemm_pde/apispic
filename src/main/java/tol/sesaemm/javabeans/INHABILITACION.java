/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class INHABILITACION
  {
    private String plazo;
    private String fechaInicial;
    private String fechaFinal;

    public String getPlazo()
      {
        return plazo;
      }

    public void setPlazo(String plazo)
      {
        this.plazo = plazo;
      }

    public String getFechaInicial()
      {
        return fechaInicial;
      }

    public void setFechaInicial(String fechaInicial)
      {
        this.fechaInicial = fechaInicial;
      }

    public String getFechaFinal()
      {
        return fechaFinal;
      }

    public void setFechaFinal(String fechaFinal)
      {
        this.fechaFinal = fechaFinal;
      }

  }
