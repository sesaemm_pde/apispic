/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class MUNICIPIO_PDE
  {

    //public String id;
    public String cve_agee;
    public String nom_agee;
    public String cve_agem;
    public String nom_agem;
    private String nom_mun;
    private String cve_mun;
    
    public String getCve_agee()
      {
        return cve_agee;
      }

    public void setCve_agee(String cve_agee)
      {
        this.cve_agee = cve_agee;
      }

    public String getNom_agee()
      {
        return nom_agee;
      }

    public void setNom_agee(String nom_agee)
      {
        this.nom_agee = nom_agee;
      }

    public String getCve_agem()
      {
        return cve_agem;
      }

    public void setCve_agem(String cve_agem)
      {
        this.cve_agem = cve_agem;
      }

    public String getNom_agem()
      {
        return nom_agem;
      }

    public void setNom_agem(String nom_agem)
      {
        this.nom_agem = nom_agem;
      }

    public String getNom_mun()
      {
        return nom_mun;
      }

    public void setNom_mun(String nom_mun)
      {
        this.nom_mun = nom_mun;
      }

    public String getCve_mun()
      {
        return cve_mun;
      }

    public void setCve_mun(String cve_mun)
      {
        this.cve_mun = cve_mun;
      }
    
  }
