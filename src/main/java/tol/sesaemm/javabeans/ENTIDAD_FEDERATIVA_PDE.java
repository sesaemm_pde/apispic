/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class ENTIDAD_FEDERATIVA_PDE
  {
    
    //private String id;
    private String cve_agee;
    private String nom_agee;
    private String abrev_agee;
    private String nom_ent;
    private String cve_ent;

    public String getCve_agee()
      {
        return cve_agee;
      }

    public void setCve_agee(String cve_agee)
      {
        this.cve_agee = cve_agee;
      }

    public String getNom_agee()
      {
        return nom_agee;
      }

    public void setNom_agee(String nom_agee)
      {
        this.nom_agee = nom_agee;
      }

    public String getAbrev_agee()
      {
        return abrev_agee;
      }

    public void setAbrev_agee(String abrev_agee)
      {
        this.abrev_agee = abrev_agee;
      }

    public String getNom_ent()
      {
        return nom_ent;
      }

    public void setNom_ent(String nom_ent)
      {
        this.nom_ent = nom_ent;
      }

    public String getCve_ent()
      {
        return cve_ent;
      }

    public void setCve_ent(String cve_ent)
      {
        this.cve_ent = cve_ent;
      }

  }
