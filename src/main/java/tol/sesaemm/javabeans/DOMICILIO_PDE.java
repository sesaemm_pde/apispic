/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;


/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DOMICILIO_PDE
  {
    private PAIS pais;
    private ENTIDAD_FEDERATIVA_PDE entidad_federativa;
    private MUNICIPIO_PDE municipio;
    private String cp;
    private LOCALIDAD_PDE localidad;
    private VIALIDAD_PDE vialidad;
    private String numExt;
    private String numInt;

    public PAIS getPais()
      {
        return pais;
      }

    public void setPais(PAIS pais)
      {
        this.pais = pais;
      }

    public ENTIDAD_FEDERATIVA_PDE getEntidad_federativa()
      {
        return entidad_federativa;
      }

    public void setEntidad_federativa(ENTIDAD_FEDERATIVA_PDE entidad_federativa)
      {
        this.entidad_federativa = entidad_federativa;
      }

    public MUNICIPIO_PDE getMunicipio()
      {
        return municipio;
      }

    public void setMunicipio(MUNICIPIO_PDE municipio)
      {
        this.municipio = municipio;
      }

    public String getCp()
      {
        return cp;
      }

    public void setCp(String cp)
      {
        this.cp = cp;
      }

    public LOCALIDAD_PDE getLocalidad()
      {
        return localidad;
      }

    public void setLocalidad(LOCALIDAD_PDE localidad)
      {
        this.localidad = localidad;
      }

    public VIALIDAD_PDE getVialidad()
      {
        return vialidad;
      }

    public void setVialidad(VIALIDAD_PDE vialidad)
      {
        this.vialidad = vialidad;
      }

    public String getNumExt()
      {
        return numExt;
      }

    public void setNumExt(String numExt)
      {
        this.numExt = numExt;
      }

    public String getNumInt()
      {
        return numInt;
      }

    public void setNumInt(String numInt)
      {
        this.numInt = numInt;
      }

  }
