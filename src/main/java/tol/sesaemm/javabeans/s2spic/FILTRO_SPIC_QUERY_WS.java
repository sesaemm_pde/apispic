/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s2spic;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class FILTRO_SPIC_QUERY_WS implements Serializable {

    private String id;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private String curp;
    private String rfc;
    private String institucionDependencia;
    private ArrayList<Integer> tipoProcedimiento; 
    private String rfcSolicitante;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getInstitucionDependencia() {
        return institucionDependencia;
    }

    public void setInstitucionDependencia(String institucionDependencia) {
        this.institucionDependencia = institucionDependencia;
    }

    public ArrayList<Integer> getTipoProcedimiento() {
        return tipoProcedimiento;
    }

    public void setTipoProcedimiento(ArrayList<Integer> tipoProcedimiento) {
        this.tipoProcedimiento = tipoProcedimiento;
    }

    public String getRfcSolicitante() {
        return rfcSolicitante;
    }

    public void setRfcSolicitante(String rfcSolicitante) {
        this.rfcSolicitante = rfcSolicitante;
    }

}
