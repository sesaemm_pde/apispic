/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s2spic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan
 * Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class SPIC implements Serializable
{

    @BsonId
    @BsonProperty("_id")
    @JsonIgnore                                                                 // anotacion para jackson
    @Exclude                                                                    // anotacion personalizada para gson
    private ObjectId idSpdn;
    private String id;
    private String fechaCaptura;
    private String ejercicioFiscal;
    private RAMO_SPIC ramo;
    private String rfc;
    private String curp;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private GENERO genero;
    private INSTITUCION_DEPENDENCIA institucionDependencia;
    private PUESTO_SPIC puesto;
    private ArrayList<TIPO_AREA> tipoArea;
    private ArrayList<NIVEL_RESPONSABILIDAD> nivelResponsabilidad;
    private ArrayList<TIPO_PROCEDIMIENTO> tipoProcedimiento;
    private SUPERIOR_INMEDIATO superiorInmediato;
    private String observaciones;
    @JsonIgnore                                                                 // anotacion para jackson
    private String dependencia;
    @JsonIgnore                                                                 // anotacion para jackson
    private METADATOS metadatos;
    private METADATA metadata;
    @JsonIgnore                                                                 // anotacion para jackson
    private String publicar;

    public SPIC()
    {
    }

    public ObjectId getIdSpdn()
    {
        return idSpdn;
    }

    public void setIdSpdn(ObjectId idSpdn)
    {
        this.idSpdn = idSpdn;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFechaCaptura()
    {
        return fechaCaptura;
    }

    public void setFechaCaptura(String fechaCaptura)
    {
        this.fechaCaptura = fechaCaptura;
    }

    public String getEjercicioFiscal()
    {
        return ejercicioFiscal;
    }

    public void setEjercicioFiscal(String ejercicioFiscal)
    {
        this.ejercicioFiscal = ejercicioFiscal;
    }

    public RAMO_SPIC getRamo()
    {
        return ramo;
    }

    public void setRamo(RAMO_SPIC ramo)
    {
        this.ramo = ramo;
    }

    public String getRfc()
    {
        return rfc;
    }

    public void setRfc(String rfc)
    {
        this.rfc = rfc;
    }

    public String getCurp()
    {
        return curp;
    }

    public void setCurp(String curp)
    {
        this.curp = curp;
    }

    public String getNombres()
    {
        return nombres;
    }

    public void setNombres(String nombres)
    {
        this.nombres = nombres;
    }

    public String getPrimerApellido()
    {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido)
    {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido()
    {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido)
    {
        this.segundoApellido = segundoApellido;
    }

    public GENERO getGenero()
    {
        return genero;
    }

    public void setGenero(GENERO genero)
    {
        this.genero = genero;
    }

    public INSTITUCION_DEPENDENCIA getInstitucionDependencia()
    {
        return institucionDependencia;
    }

    public void setInstitucionDependencia(INSTITUCION_DEPENDENCIA institucionDependencia)
    {
        this.institucionDependencia = institucionDependencia;
    }

    public PUESTO_SPIC getPuesto()
    {
        return puesto;
    }

    public void setPuesto(PUESTO_SPIC puesto)
    {
        this.puesto = puesto;
    }

    public ArrayList<TIPO_AREA> getTipoArea()
    {
        return tipoArea;
    }

    public void setTipoArea(ArrayList<TIPO_AREA> tipoArea)
    {
        this.tipoArea = tipoArea;
    }

    public ArrayList<NIVEL_RESPONSABILIDAD> getNivelResponsabilidad()
    {
        return nivelResponsabilidad;
    }

    public void setNivelResponsabilidad(ArrayList<NIVEL_RESPONSABILIDAD> nivelResponsabilidad)
    {
        this.nivelResponsabilidad = nivelResponsabilidad;
    }

    public ArrayList<TIPO_PROCEDIMIENTO> getTipoProcedimiento()
    {
        return tipoProcedimiento;
    }

    public void setTipoProcedimiento(ArrayList<TIPO_PROCEDIMIENTO> tipoProcedimiento)
    {
        this.tipoProcedimiento = tipoProcedimiento;
    }

    public SUPERIOR_INMEDIATO getSuperiorInmediato()
    {
        return superiorInmediato;
    }

    public void setSuperiorInmediato(SUPERIOR_INMEDIATO superiorInmediato)
    {
        this.superiorInmediato = superiorInmediato;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public METADATOS getMetadatos()
    {
        return metadatos;
    }

    public void setMetadatos(METADATOS metadatos)
    {
        this.metadatos = metadatos;
    }

    public METADATA getMetadata()
    {
        return metadata;
    }

    public void setMetadata(METADATA metadata)
    {
        this.metadata = metadata;
    }

    public String getPublicar()
    {
        return publicar;
    }

    public void setPublicar(String publicar)
    {
        this.publicar = publicar;
    }
}
