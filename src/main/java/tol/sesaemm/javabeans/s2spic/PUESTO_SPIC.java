/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s2spic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import tol.sesaemm.annotations.Exclude;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class PUESTO_SPIC implements Serializable
  {
    @BsonIgnore
    @JsonIgnore                                                                 // anotacion para jackson
    @Exclude                                                                    // anotacion personalizada para gson
    private String identificador;
    private String nombre;
    private String nivel;

    public String getIdentificador()
      {
        return identificador;
      }

    public void setIdentificador(String identificador)
      {
        this.identificador = identificador;
      }

    public String getNombre()
      {
        return nombre;
      }

    public void setNombre(String nombre)
      {
        this.nombre = nombre;
      }

    public String getNivel()
      {
        return nivel;
      }

    public void setNivel(String nivel)
      {
        this.nivel = nivel;
      }

    
  }
