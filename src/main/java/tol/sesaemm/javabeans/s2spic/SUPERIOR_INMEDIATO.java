/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s2spic;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class SUPERIOR_INMEDIATO implements Serializable
  {
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private String curp;
    private String rfc;
    private PUESTO_SPIC puesto;

    public String getNombres()
      {
        return nombres;
      }

    public void setNombres(String nombres)
      {
        this.nombres = nombres;
      }

    public String getPrimerApellido()
      {
        return primerApellido;
      }

    public void setPrimerApellido(String primerApellido)
      {
        this.primerApellido = primerApellido;
      }

    public String getSegundoApellido()
      {
        return segundoApellido;
      }

    public void setSegundoApellido(String segundoApellido)
      {
        this.segundoApellido = segundoApellido;
      }

    public String getCurp()
      {
        return curp;
      }

    public void setCurp(String curp)
      {
        this.curp = curp;
      }

    public String getRfc()
      {
        return rfc;
      }

    public void setRfc(String rfc)
      {
        this.rfc = rfc;
      }

    public PUESTO_SPIC getPuesto()
      {
        return puesto;
      }

    public void setPuesto(PUESTO_SPIC puesto)
      {
        this.puesto = puesto;
      }
    
  }
