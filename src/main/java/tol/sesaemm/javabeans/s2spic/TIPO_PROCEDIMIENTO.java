/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s2spic;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class TIPO_PROCEDIMIENTO implements Serializable
  {
    
    private Integer clave;
    private String valor;

    public Integer getClave()
      {
        return clave;
      }

    public void setClave(Integer clave)
      {
        this.clave = clave;
      }

    public String getValor()
      {
        return valor;
      }

    public void setValor(String valor)
      {
        this.valor = valor;
      }

  }
