/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s2spic;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class FILTRO_SPIC_WS implements Serializable {
    
    private FILTRO_SPIC_SORT_WS sort;
    private int page;
    private int pageSize;
    private FILTRO_SPIC_QUERY_WS query;
    
    public FILTRO_SPIC_SORT_WS getSort() {
        return sort;
    }

    public void setSort(FILTRO_SPIC_SORT_WS sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public FILTRO_SPIC_QUERY_WS getQuery() {
        return query;
    }

    public void setQuery(FILTRO_SPIC_QUERY_WS query) {
        this.query = query;
    }   
    
}
