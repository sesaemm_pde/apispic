/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class PUESTO
  {
    @BsonId
    @BsonProperty("_id")
    private ObjectId id;
    private String orden;
    private String jerarquia;
    private String nivel;
    private String funcional;
    @BsonIgnore
    private String nombre;

    public PUESTO()
      {
      }

    public PUESTO(
            @BsonProperty("_id")
            ObjectId id, 
            @BsonProperty("orden") 
            String orden,
            @BsonProperty("jerarquia") 
            String jerarquia, 
            @BsonProperty("nivel") 
            String nivel, 
            @BsonProperty("funcional") 
            String funcional
    )
      {
        this.id = id;
        this.orden = orden;
        this.jerarquia = jerarquia;
        this.nivel = nivel;
        this.funcional = funcional;
      }

    public ObjectId getId()
      {
        return id;
      }

    public void setId(ObjectId id)
      {
        this.id = id;
      }

    public String getOrden()
      {
        return orden;
      }

    public void setOrden(String orden)
      {
        this.orden = orden;
      }

    public String getJerarquia()
      {
        return jerarquia;
      }

    public void setJerarquia(String jerarquia)
      {
        this.jerarquia = jerarquia;
      }

    public String getNivel()
      {
        return nivel;
      }

    public void setNivel(String nivel)
      {
        this.nivel = nivel;
      }

    public String getFuncional()
      {
        return funcional;
      }

    public void setFuncional(String funcional)
      {
        this.funcional = funcional;
      }

    public String getNombre()
      {
        return nombre;
      }

    public void setNombre(String nombre)
      {
        this.nombre = nombre;
      }

    
  }
