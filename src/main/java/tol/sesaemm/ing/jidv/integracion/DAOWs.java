/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.Gson;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s2spic.FILTRO_SPIC_WS;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.SPICROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOWs
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost Estructura para la peticion POST
     * @param valoresDePaginacion Valores de la paginacion
     * @param strEstatusConsulta String del estatus de la consulta
     * @throws Exception
     */
    public static void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> coleccion;                                    
        FILTRO_SPIC_WS estructuraPostSPIC;                                      
        PAGINATION valoresDePaginacionSPIC;                                     
        ArrayList<Document> arregloListaFiltrosDeBusqueda;                      
        Document documentoBitacora;                                             
        Date fecha;                                                             
        SimpleDateFormat formateador;      
        ConfVariablesEjecucion confVariablesEjecucion;                                     

        confVariablesEjecucion = new ConfVariablesEjecucion();
        fecha = new Date();
        formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                coleccion = database.getCollection("bitacora_servicios_web");

                arregloListaFiltrosDeBusqueda = new ArrayList<>();
                documentoBitacora = new Document();

                if (estructuraPost instanceof FILTRO_SPIC_WS)
                { 
                    estructuraPostSPIC = (FILTRO_SPIC_WS) estructuraPost;
                    
                    if (estructuraPostSPIC.getQuery().getId() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "id")
                                .append("valor", estructuraPostSPIC.getQuery().getId())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getNombres() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "nombres")
                                .append("valor", estructuraPostSPIC.getQuery().getNombres())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getPrimerApellido() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "primerApellido")
                                .append("valor", estructuraPostSPIC.getQuery().getPrimerApellido())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getSegundoApellido() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "segundoApellido")
                                .append("valor", estructuraPostSPIC.getQuery().getSegundoApellido())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getCurp() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "curp")
                                .append("valor", estructuraPostSPIC.getQuery().getCurp())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getRfc() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "rfc")
                                .append("valor", estructuraPostSPIC.getQuery().getRfc())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getInstitucionDependencia() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "institucionDependencia")
                                .append("valor", estructuraPostSPIC.getQuery().getInstitucionDependencia())
                        );
                    }
                    if (estructuraPostSPIC.getQuery().getTipoProcedimiento() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "tipoProcedimiento")
                                .append("valor", estructuraPostSPIC.getQuery().getTipoProcedimiento())
                        );
                    }
                    
                    if (estructuraPostSPIC.getQuery().getRfcSolicitante() != null)
                    { 
                        documentoBitacora.append("datosGeneralesDeConsulta", new Document()
                                .append("fechaConsulta", formateador.format(fecha))
                                .append("rfcSolicitante", estructuraPostSPIC.getQuery().getRfcSolicitante())
                                .append("privacidadConsulta", "PRV")
                                .append("sistemaConsultado", "Servidores Públicos que Intervienen en Contrataciones")
                        );
                    } 
                    
                    else
                    { 
                        documentoBitacora.append("datosGeneralesDeConsulta", new Document()
                                .append("fechaConsulta", formateador.format(fecha))
                                .append("rfcSolicitante", (estructuraPostSPIC.getQuery().getRfcSolicitante() != null ? estructuraPostSPIC.getQuery().getRfcSolicitante() : ""))
                                .append("privacidadConsulta", "PUB")
                                .append("sistemaConsultado", "Servidores Públicos que Intervienen en Contrataciones")
                        );
                    } 
                    
                    if (valoresDePaginacion != null)
                    { 

                        valoresDePaginacionSPIC = (PAGINATION) valoresDePaginacion;

                        documentoBitacora.append("resultadosDeConsulta", new Document()
                                .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                .append("estatusConsulta", strEstatusConsulta)
                                .append("numeroDeRegistrosConsultados", valoresDePaginacionSPIC.getTotalRows())
                        );
                    } 
                    else
                    { 

                        documentoBitacora.append("resultadosDeConsulta", new Document()
                                .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                .append("estatusConsulta", strEstatusConsulta)
                                .append("numeroDeRegistrosConsultados", 0)
                        );
                    }                             
                } 
                
                coleccion.insertOne(documentoBitacora);
                
            } 

        }
        catch (Exception e)
        {
            throw new Exception("Error en bitacora de eventos: " + e.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

    }

    /**
     * Obtiene listado de usuarios token de acceso
     *
     * @return arreglo de usuarios
     * @throws Exception
     */
    public static ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    { 
        ArrayList<USUARIO_TOKEN_ACCESO> arregloUsuariosTokenAcceso;                         
        MongoClient conectarBaseDatos = null;                                               
        MongoDatabase database;                                                             
        MongoCollection<Document> collection;                                               
        ArrayList<Document> query;                                                          
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        ConfVariablesEjecucion confVariablesEjecucion;                                     

        confVariablesEjecucion = new ConfVariablesEjecucion();

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                collection = database.getCollection("usuarios_token_acceso");

                query = new ArrayList<>();
                query.add(new Document("$match", new Document("activo", new Document("$eq", 1))));

                arregloUsuariosTokenAcceso = collection.aggregate(query, USUARIO_TOKEN_ACCESO.class).collation(collation).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener usuarios token acceso: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return arregloUsuariosTokenAcceso;

    }

    
    /**
     * Obtiene listado de servidores públicos que intervienen en contrataciones
     *
     * @param estructuraPost Estructura para la peticion POST
     * @return Listado de servidores publicos que intervienen en contrataciones
     * @throws Exception
     */
    public static SPICROOT obtenerServidoresPublicosIntervienenContratacionesPost(FILTRO_SPIC_WS estructuraPost) throws Exception
    { 

        LogicaDeNegocio logicaNegocio = new LogicaDeNegocioV01();                   
        ArrayList<SPIC> listaServidoresPublicosIntervienenContrataciones;           
        SPICROOT resultadoConsulta;                                                 
        MongoClient conectarBaseDatos = null;                                       
        MongoDatabase database;                                                     
        MongoCollection<Document> coleccion;                                        
        Collation collation;                                                        
        AggregateIterable<Document> iteradorDeResultados;                           
        MongoCursor<Document> cursorDeResultados = null;                            
        Document where = new Document();                                            
        Document order = new Document();                                            
        int intNumeroDocumentosEncontrados = 0;                                     
        int intNumeroSaltos = 0;                                                    
        boolean hasNextPage = false;                                                
        PAGINATION spicPagination;                                                  
        Document documentoCondicionTipoProcedimiento;                               
        ArrayList<Document> arregloDocumentoCondicionesTipoProcedimiento;   
        ConfVariablesEjecucion confVariablesEjecucion;                                     

        confVariablesEjecucion = new ConfVariablesEjecucion();        

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                coleccion = database.getCollection("spic");

                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                
                arregloDocumentoCondicionesTipoProcedimiento = new ArrayList();
                documentoCondicionTipoProcedimiento = new Document();

                if (estructuraPost.getQuery().getId() != null && !estructuraPost.getQuery().getId().isEmpty())
                { 
                    where.append("identificador", estructuraPost.getQuery().getId());
                } 
                else
                { 

                    if (estructuraPost.getQuery().getNombres() != null && !estructuraPost.getQuery().getNombres().isEmpty())
                    { 
                        where.append("nombres",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getNombres())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getPrimerApellido() != null && !estructuraPost.getQuery().getPrimerApellido().isEmpty())
                    { 
                        where.append("primerApellido",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getPrimerApellido())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getSegundoApellido() != null && !estructuraPost.getQuery().getSegundoApellido().isEmpty())
                    { 
                        where.append("segundoApellido",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getSegundoApellido())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getCurp() != null && !estructuraPost.getQuery().getCurp().isEmpty())
                    { 
                        where.append("curp",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getCurp())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getRfc() != null && !estructuraPost.getQuery().getRfc().isEmpty())
                    { 
                        where.append("rfc",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getRfc())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getInstitucionDependencia() != null && !estructuraPost.getQuery().getInstitucionDependencia().isEmpty())
                    { 
                        where.append("institucionDependencia.nombre",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(estructuraPost.getQuery().getInstitucionDependencia())).append("$options", "i"));
                    } 

                    if (estructuraPost.getQuery().getTipoProcedimiento() != null && !estructuraPost.getQuery().getTipoProcedimiento().isEmpty())
                    { 
                        for (int i = 0; i < estructuraPost.getQuery().getTipoProcedimiento().size(); i++)
                        { 
                            arregloDocumentoCondicionesTipoProcedimiento.add(new Document("$eq", Arrays.asList("$$d.clave", estructuraPost.getQuery().getTipoProcedimiento().get(i))));
                        } 
                        documentoCondicionTipoProcedimiento.append("$or", arregloDocumentoCondicionesTipoProcedimiento);
                        where.append("tamanioArregloTipoProcedimientoQuery", new Document("$gt", 0));
                    } 
                } 

                where.append("publicar", new Document("$ne", "0"));
                
                if (estructuraPost.getSort().getNombres() != null && !estructuraPost.getSort().getNombres().isEmpty())
                { 
                    if (estructuraPost.getSort().getNombres().toLowerCase().equals("desc"))
                    {
                        order.append("data.nombres", -1);
                    }
                    else if (estructuraPost.getSort().getNombres().toLowerCase().equals("asc"))
                    {
                        order.append("data.nombres", 1);
                    }
                } 

                if (estructuraPost.getSort().getPrimerApellido() != null && !estructuraPost.getSort().getPrimerApellido().isEmpty())
                { 
                    if (estructuraPost.getSort().getPrimerApellido().toLowerCase().equals("desc"))
                    {
                        order.append("data.primerApellido", -1);
                    }
                    else if (estructuraPost.getSort().getPrimerApellido().toLowerCase().equals("asc"))
                    {
                        order.append("data.primerApellido", 1);
                    }
                } 

                if (estructuraPost.getSort().getSegundoApellido() != null && !estructuraPost.getSort().getSegundoApellido().isEmpty())
                { 
                    if (estructuraPost.getSort().getSegundoApellido().toLowerCase().equals("desc"))
                    {
                        order.append("data.segundoApellido", -1);
                    }
                    else if (estructuraPost.getSort().getSegundoApellido().toLowerCase().equals("asc"))
                    {
                        order.append("data.segundoApellido", 1);
                    }
                } 

                if (estructuraPost.getSort().getInstitucionDependencia() != null && !estructuraPost.getSort().getInstitucionDependencia().isEmpty())
                { 
                    if (estructuraPost.getSort().getInstitucionDependencia().toLowerCase().equals("desc"))
                    {
                        order.append("data.institucionDependencia.nombre", -1);
                    }
                    else if (estructuraPost.getSort().getInstitucionDependencia().toLowerCase().equals("asc"))
                    {
                        order.append("data.institucionDependencia.nombre", 1);
                    }
                } 

                if (estructuraPost.getSort().getPuesto() != null && !estructuraPost.getSort().getPuesto().isEmpty())
                { 
                    if (estructuraPost.getSort().getPuesto().toLowerCase().equals("desc"))
                    {
                        order.append("data.puesto.nombre", -1);
                    }
                    else if (estructuraPost.getSort().getPuesto().toLowerCase().equals("asc"))
                    {
                        order.append("data.puesto.nombre", 1);
                    }
                } 

                if (order.isEmpty())
                { 
                    order.append("data.fechaCaptura", 1);
                } 
                
                iteradorDeResultados = coleccion.aggregate(
                        Arrays.asList(
                                new Document("$addFields", new Document()
                                        .append("fechaCaptura", "$fechaCaptura")
                                        .append("ejercicioFiscal", "$ejercicioFiscal")
                                        .append("identificador", new Document()
                                                .append("$convert", new Document()
                                                        .append("input", "$_id")
                                                        .append("to", "string")
                                                        .append("onError", "")
                                                        .append("onNull", "")
                                                )
                                        )
                                        .append("arregloTipoProcedimientoQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$tipoProcedimiento")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionTipoProcedimiento)
                                                )
                                        )
                                ),
                                
                                new Document("$addFields", new Document()
                                        .append("tamanioArregloTipoProcedimientoQuery", new Document()
                                                .append("$size", "$arregloTipoProcedimientoQuery")
                                        )
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$count", "totalCount")
                        )
                ).collation(collation).allowDiskUse(true);
                
                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    intNumeroDocumentosEncontrados = cursorDeResultados.next().getInteger("totalCount");
                } 
                
                if (estructuraPost.getPage() > 1)
                { 
                    intNumeroSaltos = (int) ((estructuraPost.getPage() - 1) * estructuraPost.getPageSize());
                } 

                if ((estructuraPost.getPage() * estructuraPost.getPageSize()) < intNumeroDocumentosEncontrados)
                { 
                    hasNextPage = true;
                } 

                
                listaServidoresPublicosIntervienenContrataciones = coleccion.aggregate(
                        Arrays.asList(
                                new Document("$addFields", new Document()
                                        .append("fechaCaptura", "$fechaCaptura")
                                        .append("ejercicioFiscal", "$ejercicioFiscal")
                                        .append("identificador", new Document()
                                                .append("$convert", new Document()
                                                        .append("input", "$_id")
                                                        .append("to", "string")
                                                        .append("onError", "")
                                                        .append("onNull", "")
                                                )
                                        )
                                        .append("arregloTipoProcedimientoQuery", new Document()
                                                .append("$filter", new Document()
                                                        .append("input", "$tipoProcedimiento")
                                                        .append("as", "d")
                                                        .append("cond", documentoCondicionTipoProcedimiento)
                                                )
                                        )
                                ),
                                
                                new Document("$addFields", new Document()
                                        .append("tamanioArregloTipoProcedimientoQuery", new Document()
                                                .append("$size", "$arregloTipoProcedimientoQuery")
                                        )
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$project",
                                        new Document("data", "$$ROOT")
                                ),
                                new Document("$sort",
                                        order
                                ),
                                new Document("$skip", intNumeroSaltos),
                                new Document("$limit", estructuraPost.getPageSize()),
                                new Document("$replaceRoot",
                                        new Document("newRoot", "$data")
                                )
                        ), SPIC.class
                ).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                
                spicPagination = new PAGINATION();
                spicPagination.setPage(estructuraPost.getPage());
                spicPagination.setPageSize(estructuraPost.getPageSize());
                spicPagination.setTotalRows(intNumeroDocumentosEncontrados);
                spicPagination.setHasNextPage(hasNextPage);
                
                resultadoConsulta = new SPICROOT();
                resultadoConsulta.setPagination(spicPagination);
                resultadoConsulta.setResults(listaServidoresPublicosIntervienenContrataciones);
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener los servidores que intervienen en contrataciones: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return resultadoConsulta;

    } 

    /**
     * Obtiene estatus de privacidad de los campos de servidores publicos que
     * intervienen en contrataciones
     *
     * @param coleccion Nombre de la coleccion
     * @return Estatus de privacidad de los campos de servidores publicos que
     * intervienen en contrataciones
     * @throws Exception
     */
    public static SPIC obtenerEstatusDePrivacidadCamposServidoresPublicosIntervienenContrataciones(String coleccion) throws Exception
    { 

        SPIC estatusDePrivacidad = new SPIC();              
        MongoClient conectarBaseDatos = null;               
        MongoDatabase database;                             
        MongoCollection<Document> collection;               
        Document documentoCamposPublicosSPIC;    
        ConfVariablesEjecucion confVariablesEjecucion;                                     

        confVariablesEjecucion = new ConfVariablesEjecucion();           

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                collection = database.getCollection("campos_publicos");

                documentoCamposPublicosSPIC = collection.find(new Document("coleccion", coleccion)).first();
                if (documentoCamposPublicosSPIC != null)
                { 
                    estatusDePrivacidad = new Gson().fromJson(documentoCamposPublicosSPIC.toJson(), SPIC.class);
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return estatusDePrivacidad;
    } 

    /**
     * Obtiene dependencias de servidores publicos que intervienen en
     * contrataciones
     *
     * @return Dependencias de servidores publicos que intervienen en
     * contrataciones
     * @throws Exception
     */
    public static ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasServidoresPublicosIntervienenContrataciones() throws Exception
    { 

        ArrayList<INSTITUCION_DEPENDENCIA> listadoInstitucionDependencia;       
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Collation collation;  
        ConfVariablesEjecucion confVariablesEjecucion;                                     

        confVariablesEjecucion = new ConfVariablesEjecucion();                                                  

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();
            listadoInstitucionDependencia = new ArrayList<>();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                collection = database.getCollection("spic");

                
                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                

                
                listadoInstitucionDependencia = collection.aggregate(
                        Arrays.asList(
                                new Document("$project", new Document("nombre", "$institucionDependencia.nombre")
                                        .append("siglas", "$institucionDependencia.siglas")
                                        .append("clave", "$institucionDependencia.clave")
                                ),
                                new Document("$group",
                                        new Document("_id", new Document("nombre", "$nombre")
                                                .append("siglas", "$siglas")
                                                .append("clave", "$clave"))
                                ),
                                new Document("$sort", new Document("_id.nombre", 1)
                                ),
                                new Document("$replaceRoot",
                                        new Document("newRoot", "$_id")
                                )
                        ), INSTITUCION_DEPENDENCIA.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener las Dependencias : " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoInstitucionDependencia;

    } 
    

    /**
     * Verifica si el RFC es valido
     *
     * @param rfc RFC a validar
     * @return Verdadero o falso
     * @throws Exception
     */
    public static boolean esValidoRFC(String rfc) throws Exception
    { 

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> iteradorDeResultados;                            
        MongoCursor<Document> cursorDeResultados = null;                        
        boolean esValido = false;  
        ConfVariablesEjecucion confVariablesEjecucion;                                     

        confVariablesEjecucion = new ConfVariablesEjecucion();                                             

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariablesEjecucion.getBD());
                collection = database.getCollection("usuarios");

                iteradorDeResultados = collection.find(new Document("rfc", rfc));

                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    esValido = true;
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return esValido;

    } 

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 

        if (cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 

        if (conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 

    } 

}
