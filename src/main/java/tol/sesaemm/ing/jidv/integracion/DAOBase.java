/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.mongodb.Block;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.ClusterSettings;
import java.util.Arrays;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOBase
{

    public static MongoClient conectarBaseDatos() throws Exception
    {

        MongoCredential credential;
        MongoClient mongoClient = null;
        CodecRegistry pojoCodecRegistry;
        ConfVariablesEjecucion configuracionVariables;

        configuracionVariables = new ConfVariablesEjecucion();
        pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
                         
        credential = MongoCredential.createCredential(configuracionVariables.getUSUARIO(), configuracionVariables.getBD(), configuracionVariables.getCONTRASENIA().toCharArray());
        mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                        .applyToClusterSettings(new Block<ClusterSettings.Builder>()
                        {
                            @Override
                            public void apply(ClusterSettings.Builder builder)
                            {
                                builder.hosts(Arrays.asList(new ServerAddress(configuracionVariables.getHOST(), configuracionVariables.getPUERTO())));
                            }
                        })
                        .codecRegistry(pojoCodecRegistry)
                        .credential(credential)
                        .build());

        return mongoClient;
    }

    private static class BlockImpl implements Block<ClusterSettings.Builder>
    {

        public BlockImpl()
        {
        }

        @Override
        public void apply(ClusterSettings.Builder builder)
        {
            ConfVariablesEjecucion configuracionVariables;
            
            configuracionVariables = new ConfVariablesEjecucion();
            
            builder.hosts(Arrays.asList(new ServerAddress(configuracionVariables.getHOST(), configuracionVariables.getPUERTO())));
        }
    }
}
