/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaNegocio;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.ing.jidv.integracion.DAOWs;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s2spic.FILTRO_SPIC_QUERY_WS;
import tol.sesaemm.javabeans.s2spic.FILTRO_SPIC_SORT_WS;
import tol.sesaemm.javabeans.s2spic.FILTRO_SPIC_WS;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.SPICROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public class LogicaDeNegocioV01 implements LogicaDeNegocio
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost Estructura para la peticion POST
     * @param valoresDePaginacion Valores de la paginacion
     * @param strEstatusConsulta String del estatus de la consulta
     * @throws Exception
     */
    @Override
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    {
        DAOWs.guardarBitacoraDeEventos(estructuraPost, valoresDePaginacion, strEstatusConsulta);
    }

    /**
     * Obtener Listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    @Override
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    {
        return DAOWs.obtenerUsuariosTokenDeAcceso();
    }

    /**
     * Obtener valores por default de la estructura post
     *
     * @param estructuraPost Estructura para la peticion POST
     * @return Valores por default de la estructura post
     * @throws Exception
     */
    @Override
    public FILTRO_SPIC_WS obtenerValoresPorDefaultFiltroSpic(FILTRO_SPIC_WS estructuraPost) throws Exception
    {
        if (estructuraPost.getSort() == null)
        {
            estructuraPost.setSort(new FILTRO_SPIC_SORT_WS());
        }
        if (estructuraPost.getPage() <= 0)
        {
            estructuraPost.setPage(1);
        }
        if (estructuraPost.getPageSize() <= 0)
        {
            estructuraPost.setPageSize(10);
        }
        else if (estructuraPost.getPageSize() > 200)
        {
            estructuraPost.setPageSize(200);
        }
        if (estructuraPost.getQuery() == null)
        {
            estructuraPost.setQuery(new FILTRO_SPIC_QUERY_WS());
        }

        return estructuraPost;
    }

    /**
     * Obtener listado de servidores publicos que intervienen en contrataciones
     *
     * @param estructuraPost Estructura para la peticion POST
     * @return Listado de servidores publicos que intervienen en contrataciones
     * @throws Exception
     */
    @Override
    public SPICROOT obtenerServidoresPublicosIntervienenContratacionesPost(FILTRO_SPIC_WS estructuraPost) throws Exception
    {
        return DAOWs.obtenerServidoresPublicosIntervienenContratacionesPost(estructuraPost);
    }

    /**
     * Obtener estatus de privacidad de los campos para servidores publicos que
     * intervienen en contrataciones
     *
     * @return Privacidad de los campos para servidores publicos que intervienen
     * en contrataciones
     * @throws Exception
     */
    @Override
    public SPIC obtenerEstatusDePrivacidadCamposServidoresPublicosIntervienenContrataciones() throws Exception
    {
        return DAOWs.obtenerEstatusDePrivacidadCamposServidoresPublicosIntervienenContrataciones("contrataciones");
    }

    /**
     * Convertir a formato JSON estructura POST
     *
     * @param estructuraServidoresPublicosIntervienenContrataciones Objeto que
     * almacena la estructura del sistema SPIC
     * @param camposPublicos Objeto que almacena la privacidad de los campos del
     * sistema SPIC
     * @param rfcSolicitante Almacena el RFC del solicitante
     * @return Formato JSON estructura POST
     * @throws Exception
     */
    @Override
    public String generarJsonServidoresPublicosIntervienenContrataciones(SPICROOT estructuraServidoresPublicosIntervienenContrataciones, SPIC camposPublicos, String rfcSolicitante) throws Exception
    {

        PAGINATION atributosDePaginacion;
        ArrayList<SPIC> servidoresPublicos;
        Document documentoServidoresPublicos;
        List<Document> listaDocumentosServidoresPublicos;
        List<Document> arregloTipoArea;
        List<Document> arregloNivelResponsabilidad;
        List<Document> arregloTipoProcedimiento;

        atributosDePaginacion = estructuraServidoresPublicosIntervienenContrataciones.getPagination();
        servidoresPublicos = estructuraServidoresPublicosIntervienenContrataciones.getResults();

        listaDocumentosServidoresPublicos = new ArrayList<>();

        for (SPIC servidorPublico : servidoresPublicos)
        {

            documentoServidoresPublicos = new Document();

            documentoServidoresPublicos.append("id", servidorPublico.getIdSpdn().toHexString());

            if (rfcSolicitante != null && !rfcSolicitante.isEmpty())
            {
                documentoServidoresPublicos.append("fechaCaptura", servidorPublico.getFechaCaptura());
                if (servidorPublico.getEjercicioFiscal() != null)
                {
                    documentoServidoresPublicos.append("ejercicioFiscal", servidorPublico.getEjercicioFiscal());
                }
                if (servidorPublico.getRamo() != null)
                {
                    Document documentoRamo = new Document();
                    if (servidorPublico.getRamo().getClave() != null)
                    {
                        documentoRamo.append("clave", servidorPublico.getRamo().getClave());
                    }
                    if (servidorPublico.getRamo().getValor() != null)
                    {
                        documentoRamo.append("valor", servidorPublico.getRamo().getValor());
                    }
                    if (!documentoRamo.isEmpty())
                    {
                        documentoServidoresPublicos.append("ramo", documentoRamo);
                    }
                }
                if (servidorPublico.getRfc() != null)
                {
                    documentoServidoresPublicos.append("rfc", servidorPublico.getRfc());
                }
                if (servidorPublico.getCurp() != null)
                {
                    documentoServidoresPublicos.append("curp", servidorPublico.getCurp());
                }
                documentoServidoresPublicos.append("nombres", servidorPublico.getNombres());
                documentoServidoresPublicos.append("primerApellido", servidorPublico.getPrimerApellido());
                if (servidorPublico.getSegundoApellido() != null)
                {
                    documentoServidoresPublicos.append("segundoApellido", servidorPublico.getSegundoApellido());
                }
                if (servidorPublico.getGenero() != null)
                {
                    Document documentoGenero = new Document();
                    if (servidorPublico.getGenero().getClave() != null)
                    {
                        documentoGenero.append("clave", servidorPublico.getGenero().getClave());
                    }
                    if (servidorPublico.getGenero().getValor() != null)
                    {
                        documentoGenero.append("valor", servidorPublico.getGenero().getValor());
                    }
                    if (!documentoGenero.isEmpty())
                    {
                        documentoServidoresPublicos.append("genero", documentoGenero);
                    }
                }
                if (servidorPublico.getInstitucionDependencia() == null)
                {
                    documentoServidoresPublicos.append("institucionDependencia", new Document()
                            .append("nombre", null)
                    );
                }
                else
                {
                    Document documentoInstitucionDependencia = new Document();
                    documentoInstitucionDependencia.append("nombre", servidorPublico.getInstitucionDependencia().getNombre());
                    if (servidorPublico.getInstitucionDependencia().getSiglas() != null)
                    {
                        documentoInstitucionDependencia.append("siglas", servidorPublico.getInstitucionDependencia().getSiglas());
                    }
                    if (servidorPublico.getInstitucionDependencia().getClave() != null)
                    {
                        documentoInstitucionDependencia.append("clave", servidorPublico.getInstitucionDependencia().getClave());
                    }
                    documentoServidoresPublicos.append("institucionDependencia", documentoInstitucionDependencia);
                }
                if (servidorPublico.getGenero() == null)
                {
                    documentoServidoresPublicos.append("puesto", new Document()
                            .append("nombre", null)
                            .append("nivel", null)
                    );
                }
                else
                {
                    documentoServidoresPublicos.append("puesto", new Document()
                            .append("nombre", servidorPublico.getPuesto().getNombre())
                            .append("nivel", servidorPublico.getPuesto().getNivel())
                    );
                }
                arregloTipoArea = new ArrayList<>();
                if (servidorPublico.getTipoArea() != null)
                {
                    for (int j = 0; j < servidorPublico.getTipoArea().size(); j++)
                    {
                        Document documentoTipoArea = new Document();
                        if (servidorPublico.getTipoArea().get(j).getClave() != null)
                        {
                            documentoTipoArea.append("clave", servidorPublico.getTipoArea().get(j).getClave());
                        }
                        if (servidorPublico.getTipoArea().get(j).getValor() != null)
                        {
                            documentoTipoArea.append("valor", servidorPublico.getTipoArea().get(j).getValor());
                        }
                        if (!documentoTipoArea.isEmpty())
                        {
                            arregloTipoArea.add(documentoTipoArea);
                        }
                    }
                }
                if (!arregloTipoArea.isEmpty())
                {
                    documentoServidoresPublicos.append("tipoArea", arregloTipoArea);
                }
                arregloNivelResponsabilidad = new ArrayList<>();
                if (servidorPublico.getNivelResponsabilidad() != null)
                {
                    for (int j = 0; j < servidorPublico.getNivelResponsabilidad().size(); j++)
                    {
                        Document documentoNivelResponsabilidad = new Document();
                        if (servidorPublico.getNivelResponsabilidad().get(j).getClave() != null)
                        {
                            documentoNivelResponsabilidad.append("clave", servidorPublico.getNivelResponsabilidad().get(j).getClave());
                        }
                        if (servidorPublico.getNivelResponsabilidad().get(j).getValor() != null)
                        {
                            documentoNivelResponsabilidad.append("valor", servidorPublico.getNivelResponsabilidad().get(j).getValor());
                        }
                        if (!documentoNivelResponsabilidad.isEmpty())
                        {
                            arregloNivelResponsabilidad.add(documentoNivelResponsabilidad);
                        }
                    }
                }
                if (!arregloNivelResponsabilidad.isEmpty())
                {
                    documentoServidoresPublicos.append("nivelResponsabilidad", arregloNivelResponsabilidad);
                }

                if (servidorPublico.getTipoProcedimiento() != null)
                {
                    arregloTipoProcedimiento = new ArrayList<>();
                    for (int j = 0; j < servidorPublico.getTipoProcedimiento().size(); j++)
                    {
                        Document documentoTipoProcedimiento = new Document();
                        documentoTipoProcedimiento.append("clave", servidorPublico.getTipoProcedimiento().get(j).getClave());
                        documentoTipoProcedimiento.append("valor", servidorPublico.getTipoProcedimiento().get(j).getValor());
                        arregloTipoProcedimiento.add(documentoTipoProcedimiento);
                    }
                    documentoServidoresPublicos.append("tipoProcedimiento", arregloTipoProcedimiento);
                }
                else
                {
                    documentoServidoresPublicos.append("tipoProcedimiento", null);
                }
                if (servidorPublico.getSuperiorInmediato() != null)
                {
                    Document documentoSuperiorInmediato = new Document();
                    Document documentoPuesto = new Document();

                    if (servidorPublico.getSuperiorInmediato().getNombres() != null)
                    {
                        documentoSuperiorInmediato.append("nombres", servidorPublico.getSuperiorInmediato().getNombres());
                    }
                    if (servidorPublico.getSuperiorInmediato().getPrimerApellido() != null)
                    {
                        documentoSuperiorInmediato.append("primerApellido", servidorPublico.getSuperiorInmediato().getPrimerApellido());
                    }
                    if (servidorPublico.getSuperiorInmediato().getSegundoApellido() != null)
                    {
                        documentoSuperiorInmediato.append("segundoApellido", servidorPublico.getSuperiorInmediato().getSegundoApellido());
                    }
                    if (servidorPublico.getSuperiorInmediato().getCurp() != null)
                    {
                        documentoSuperiorInmediato.append("curp", servidorPublico.getSuperiorInmediato().getCurp());
                    }
                    if (servidorPublico.getSuperiorInmediato().getRfc() != null)
                    {
                        documentoSuperiorInmediato.append("rfc", servidorPublico.getSuperiorInmediato().getRfc());
                    }
                    if (servidorPublico.getSuperiorInmediato().getPuesto() != null)
                    {
                        if (servidorPublico.getSuperiorInmediato().getPuesto().getNombre() != null)
                        {
                            documentoPuesto.append("nombre", servidorPublico.getSuperiorInmediato().getPuesto().getNombre());
                        }
                        if (servidorPublico.getSuperiorInmediato().getPuesto().getNivel() != null)
                        {
                            documentoPuesto.append("nivel", servidorPublico.getSuperiorInmediato().getPuesto().getNivel());
                        }
                        if (!documentoPuesto.isEmpty())
                        {
                            documentoSuperiorInmediato.append("puesto", documentoPuesto);
                        }
                    }
                    if (!documentoSuperiorInmediato.isEmpty())
                    {
                        documentoServidoresPublicos.append("superiorInmediato", documentoSuperiorInmediato);
                    }
                }

                if (servidorPublico.getObservaciones() != null)
                {
                    documentoServidoresPublicos.append("observaciones", servidorPublico.getObservaciones());
                }

            }
            else
            {
                documentoServidoresPublicos.append("fechaCaptura", servidorPublico.getFechaCaptura());
                if (camposPublicos.getEjercicioFiscal().equals("1") && servidorPublico.getEjercicioFiscal() != null)
                {
                    documentoServidoresPublicos.append("ejercicioFiscal", servidorPublico.getEjercicioFiscal());
                }
                if (servidorPublico.getRamo() != null)
                {
                    Document documentoRamo = new Document();

                    if (camposPublicos.getRamo().getClave() == 1 && servidorPublico.getRamo().getClave() != null)
                    {
                        documentoRamo.append("clave", servidorPublico.getRamo().getClave());
                    }
                    if (camposPublicos.getRamo().getValor().equals("1") && servidorPublico.getRamo().getValor() != null)
                    {
                        documentoRamo.append("valor", servidorPublico.getRamo().getValor());
                    }

                    if (!documentoRamo.isEmpty())
                    {
                        documentoServidoresPublicos.append("ramo", documentoRamo);
                    }
                }
                if (camposPublicos.getRfc().equals("1") && servidorPublico.getRfc() != null)
                {
                    documentoServidoresPublicos.append("rfc", servidorPublico.getRfc());
                }

                if (camposPublicos.getCurp().equals("1") && servidorPublico.getCurp() != null)
                {
                    documentoServidoresPublicos.append("curp", servidorPublico.getCurp());
                }

                documentoServidoresPublicos.append("nombres", servidorPublico.getNombres());

                documentoServidoresPublicos.append("primerApellido", servidorPublico.getPrimerApellido());

                if (camposPublicos.getSegundoApellido().equals("1") && servidorPublico.getSegundoApellido() != null)
                {
                    documentoServidoresPublicos.append("segundoApellido", servidorPublico.getSegundoApellido());
                }

                if (servidorPublico.getGenero() != null)
                {
                    Document documentoGenero = new Document();

                    if (camposPublicos.getGenero().getClave().equals("1") && servidorPublico.getGenero().getClave() != null)
                    {
                        documentoGenero.append("clave", servidorPublico.getGenero().getClave());
                    }
                    if (camposPublicos.getGenero().getValor().equals("1") && servidorPublico.getGenero().getValor() != null)
                    {
                        documentoGenero.append("valor", servidorPublico.getGenero().getValor());
                    }

                    if (!documentoGenero.isEmpty())
                    {
                        documentoServidoresPublicos.append("genero", documentoGenero);
                    }
                }

                Document documentoInstitucionDependencia = new Document();
                if (servidorPublico.getInstitucionDependencia() == null)
                {
                    documentoInstitucionDependencia.append("nombre", null);
                }
                else
                {
                    documentoInstitucionDependencia.append("nombre", servidorPublico.getInstitucionDependencia().getNombre());

                    if (camposPublicos.getInstitucionDependencia().getSiglas().equals("1") && servidorPublico.getInstitucionDependencia().getSiglas() != null)
                    {
                        documentoInstitucionDependencia.append("siglas", servidorPublico.getInstitucionDependencia().getSiglas());
                    }
                    if (camposPublicos.getInstitucionDependencia().getClave().equals("1") && servidorPublico.getInstitucionDependencia().getClave() != null)
                    {
                        documentoInstitucionDependencia.append("clave", servidorPublico.getInstitucionDependencia().getClave());
                    }
                }
                documentoServidoresPublicos.append("institucionDependencia", documentoInstitucionDependencia);

                if (servidorPublico.getPuesto() == null)
                {
                    documentoServidoresPublicos.append("puesto", new Document()
                            .append("nombre", null)
                            .append("nivel", null)
                    );
                }
                else
                {
                    documentoServidoresPublicos.append("puesto", new Document()
                            .append("nombre", servidorPublico.getPuesto().getNombre())
                            .append("nivel", servidorPublico.getPuesto().getNivel())
                    );
                }

                arregloTipoArea = new ArrayList<>();
                if (servidorPublico.getTipoArea() != null)
                {
                    for (int j = 0; j < servidorPublico.getTipoArea().size(); j++)
                    {
                        Document documentoTipoArea = new Document();

                        if (camposPublicos.getTipoArea().get(0).getClave().equals("1") && servidorPublico.getTipoArea().get(j).getClave() != null)
                        {
                            documentoTipoArea.append("clave", servidorPublico.getTipoArea().get(j).getClave());
                        }
                        if (camposPublicos.getTipoArea().get(0).getValor().equals("1") && servidorPublico.getTipoArea().get(j).getValor() != null)
                        {
                            documentoTipoArea.append("valor", servidorPublico.getTipoArea().get(j).getValor());
                        }

                        if (!documentoTipoArea.isEmpty())
                        {
                            arregloTipoArea.add(documentoTipoArea);
                        }
                    }
                }
                if (!arregloTipoArea.isEmpty())
                {
                    documentoServidoresPublicos.append("tipoArea", arregloTipoArea);
                }

                arregloNivelResponsabilidad = new ArrayList<>();
                if (servidorPublico.getNivelResponsabilidad() != null)
                {
                    for (int j = 0; j < servidorPublico.getNivelResponsabilidad().size(); j++)
                    {
                        Document documentoNivelResponsabilidad = new Document();

                        if (camposPublicos.getNivelResponsabilidad().get(0).getClave().equals("1") && servidorPublico.getNivelResponsabilidad().get(j).getClave() != null)
                        {
                            documentoNivelResponsabilidad.append("clave", servidorPublico.getNivelResponsabilidad().get(j).getClave());
                        }
                        if (camposPublicos.getNivelResponsabilidad().get(0).getValor().equals("1") && servidorPublico.getNivelResponsabilidad().get(j).getValor() != null)
                        {
                            documentoNivelResponsabilidad.append("valor", servidorPublico.getNivelResponsabilidad().get(j).getValor());
                        }

                        if (!documentoNivelResponsabilidad.isEmpty())
                        {
                            arregloNivelResponsabilidad.add(documentoNivelResponsabilidad);
                        }
                    }
                }
                if (!arregloNivelResponsabilidad.isEmpty())
                {
                    documentoServidoresPublicos.append("nivelResponsabilidad", arregloNivelResponsabilidad);
                }

                if (servidorPublico.getTipoProcedimiento() != null)
                {
                    arregloTipoProcedimiento = new ArrayList<>();
                    for (int j = 0; j < servidorPublico.getTipoProcedimiento().size(); j++)
                    {
                        Document documentoTipoProcedimiento = new Document();
                        documentoTipoProcedimiento.append("clave", servidorPublico.getTipoProcedimiento().get(j).getClave());
                        documentoTipoProcedimiento.append("valor", servidorPublico.getTipoProcedimiento().get(j).getValor());
                        arregloTipoProcedimiento.add(documentoTipoProcedimiento);
                    }
                    documentoServidoresPublicos.append("tipoProcedimiento", arregloTipoProcedimiento);
                }
                else
                {
                    documentoServidoresPublicos.append("tipoProcedimiento", null);
                }

                if (servidorPublico.getSuperiorInmediato() != null)
                {
                    Document documentoSuperiorInmmediato = new Document();
                    Document documentoPuesto = new Document();

                    if (camposPublicos.getSuperiorInmediato().getNombres().equals("1") && servidorPublico.getSuperiorInmediato().getNombres() != null)
                    {
                        documentoSuperiorInmmediato.append("nombres", servidorPublico.getSuperiorInmediato().getNombres());
                    }
                    if (camposPublicos.getSuperiorInmediato().getPrimerApellido().equals("1") && servidorPublico.getSuperiorInmediato().getPrimerApellido() != null)
                    {
                        documentoSuperiorInmmediato.append("primerApellido", servidorPublico.getSuperiorInmediato().getPrimerApellido());
                    }
                    if (camposPublicos.getSuperiorInmediato().getSegundoApellido().equals("1") && servidorPublico.getSuperiorInmediato().getSegundoApellido() != null)
                    {
                        documentoSuperiorInmmediato.append("segundoApellido", servidorPublico.getSuperiorInmediato().getSegundoApellido());
                    }
                    if (camposPublicos.getSuperiorInmediato().getCurp().equals("1") && servidorPublico.getSuperiorInmediato().getCurp() != null)
                    {
                        documentoSuperiorInmmediato.append("curp", servidorPublico.getSuperiorInmediato().getCurp());
                    }
                    if (camposPublicos.getSuperiorInmediato().getRfc().equals("1") && servidorPublico.getSuperiorInmediato().getRfc() != null)
                    {
                        documentoSuperiorInmmediato.append("rfc", servidorPublico.getSuperiorInmediato().getRfc());
                    }

                    if (servidorPublico.getSuperiorInmediato().getPuesto() != null)
                    {

                        if (camposPublicos.getSuperiorInmediato().getPuesto().getNombre().equals("1") && servidorPublico.getSuperiorInmediato().getPuesto().getNombre() != null)
                        {
                            documentoPuesto.append("nombre", servidorPublico.getSuperiorInmediato().getPuesto().getNombre());
                        }
                        if (camposPublicos.getSuperiorInmediato().getPuesto().getNivel().equals("1") && servidorPublico.getSuperiorInmediato().getPuesto().getNivel() != null)
                        {
                            documentoPuesto.append("nivel", servidorPublico.getSuperiorInmediato().getPuesto().getNivel());
                        }

                        if (!documentoPuesto.isEmpty())
                        {
                            documentoSuperiorInmmediato.append("puesto", documentoPuesto);
                        }
                    }
                    if (!documentoSuperiorInmmediato.isEmpty())
                    {
                        documentoServidoresPublicos.append("superiorInmediato", documentoSuperiorInmmediato);
                    }
                }

                if (camposPublicos.getObservaciones().equals("1") && servidorPublico.getObservaciones() != null)
                {
                    documentoServidoresPublicos.append("observaciones", servidorPublico.getObservaciones());
                }

            }

            listaDocumentosServidoresPublicos.add(documentoServidoresPublicos);
        }

        documentoServidoresPublicos = new Document();
        documentoServidoresPublicos.append("pagination",
                new Document("pageSize", atributosDePaginacion.getPageSize())
                        .append("page", atributosDePaginacion.getPage())
                        .append("totalRows", atributosDePaginacion.getTotalRows())
                        .append("hasNextPage", atributosDePaginacion.getHasNextPage()));

        documentoServidoresPublicos.append("results", listaDocumentosServidoresPublicos);

        return documentoServidoresPublicos.toJson();
    }

    /**
     * Obtener dependencias de los servidores publicos que intervienen en
     * contrataciones
     *
     * @return Lista de dependencias de los servidores publicos que intervienen
     * en contrataciones
     * @throws Exception
     */
    @Override
    public ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasServidoresPublicosIntervienenContrataciones() throws Exception
    {
        return DAOWs.obtenerDependenciasServidoresPublicosIntervienenContrataciones();
    }

    /**
     * Obtener en formato Json lista de dependencias de servidores publicos que
     * intervienen en contrataciones
     *
     * @param dependenciasServidoresPublicosIntervienenContrataciones Almacena
     * dependencias de los servidores publicos que intervienen en contrataciones
     * @return Lista en formato Json de las dependencias de los servidores
     * publicos que intervienen en contrataciones
     * @throws Exception
     */
    @Override
    public String generarJsonDependenciasServidoresPublicosIntervienenContrataciones(ArrayList<INSTITUCION_DEPENDENCIA> dependenciasServidoresPublicosIntervienenContrataciones) throws Exception
    {

        Document documentoDependenciasServidoresPublicosIntervienenContrataciones;
        List<Document> listaDocumetosDependenciasServidoresPublicosIntervienenContrataciones = new ArrayList<>();

        for (INSTITUCION_DEPENDENCIA dependencia : dependenciasServidoresPublicosIntervienenContrataciones)
        {
            documentoDependenciasServidoresPublicosIntervienenContrataciones = new Document();
            documentoDependenciasServidoresPublicosIntervienenContrataciones.append("nombre", dependencia.getNombre());
            documentoDependenciasServidoresPublicosIntervienenContrataciones.append("siglas", dependencia.getSiglas());
            documentoDependenciasServidoresPublicosIntervienenContrataciones.append("clave", dependencia.getClave());
            listaDocumetosDependenciasServidoresPublicosIntervienenContrataciones.add(documentoDependenciasServidoresPublicosIntervienenContrataciones);
        }

        return new Gson().toJson(listaDocumetosDependenciasServidoresPublicosIntervienenContrataciones);
    }

    /**
     * Convierte una cadena, en una expresión regular que contiene o no, acentos
     *
     * @param palabra
     * @return
     */
    @Override
    public String crearExpresionRegularDePalabra(String palabra)
    {

        String expReg = "";
        char[] arregloDePalabras = palabra.toLowerCase().toCharArray();

        for (char letra : arregloDePalabras)
        {
            switch (letra)
            {
                case 'a':
                    expReg += "[aáAÁ]";
                    break;
                case 'á':
                    expReg += "[aáAÁ]";
                    break;
                case 'e':
                    expReg += "[eéEÉ]";
                    break;
                case 'é':
                    expReg += "[eéEÉ]";
                    break;
                case 'i':
                    expReg += "[iíIÍ]";
                    break;
                case 'í':
                    expReg += "[iíIÍ]";
                    break;
                case 'o':
                    expReg += "[oóOÓ]";
                    break;
                case 'ó':
                    expReg += "[oóOÓ]";
                    break;
                case 'u':
                    expReg += "[uúUÚ]";
                    break;
                case 'ú':
                    expReg += "[uúUÚ]";
                    break;
                default:
                    expReg += letra;
                    break;
            }
        }

        return expReg;
    }

    /**
     * Valida RFC
     *
     * @param rfc RFC
     * @return Falso o verdadero
     * @throws Exception
     */
    @Override
    public boolean esValidoRFC(String rfc) throws Exception
    {
        return DAOWs.esValidoRFC(rfc);
    }
}
