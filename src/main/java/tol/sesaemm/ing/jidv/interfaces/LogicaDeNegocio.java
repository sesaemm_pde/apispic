/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s2spic.FILTRO_SPIC_WS;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.SPICROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public interface LogicaDeNegocio
{    
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion ,String strEstatusConsulta) throws Exception;    
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception;    
    public FILTRO_SPIC_WS obtenerValoresPorDefaultFiltroSpic (FILTRO_SPIC_WS estructuraPost) throws Exception;
    public SPICROOT obtenerServidoresPublicosIntervienenContratacionesPost(FILTRO_SPIC_WS estructuraPost) throws Exception;
    public SPIC obtenerEstatusDePrivacidadCamposServidoresPublicosIntervienenContrataciones() throws Exception;
    public String generarJsonServidoresPublicosIntervienenContrataciones(SPICROOT estructuraServidoresPublicosIntervienenContrataciones, SPIC camposPublicos, String rfcSolicitante) throws Exception;
    public ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasServidoresPublicosIntervienenContrataciones() throws Exception;
    public String generarJsonDependenciasServidoresPublicosIntervienenContrataciones(ArrayList<INSTITUCION_DEPENDENCIA> dependenciasServidoresPublicosIntervienenContrataciones) throws Exception;     
    public String crearExpresionRegularDePalabra(String palabra);   
    public boolean esValidoRFC(String rfc) throws Exception;    
}