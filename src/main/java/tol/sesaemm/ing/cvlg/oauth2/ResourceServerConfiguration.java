/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz
 * ismael.ortiz@sesaemm.org.mx
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter
{

    @Autowired
    @Qualifier("puntoEntradaAutenticacion")
    AuthenticationForEntryPoint puntoEntradaAutenticacion;

    @Autowired
    @Qualifier("controladorAccesoDenegado")
    ControllerAccessDenied controladorAccesoDenegado;

    /**
     * @param resources
     * @throws Exception *
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception
    {
        resources
                .resourceId("servicioRestSPIC")
                .stateless(false)
                .authenticationEntryPoint(puntoEntradaAutenticacion)
                .accessDeniedHandler(controladorAccesoDenegado);
    }

    /**
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .anyRequest().authenticated();
    }
}
