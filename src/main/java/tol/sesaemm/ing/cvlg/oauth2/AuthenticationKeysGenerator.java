/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz
 * ismael.ortiz@sesaemm.org.mx
 */
public class AuthenticationKeysGenerator implements AuthenticationKeyGenerator
{

    /**
     * @param authentication
     * @return String
     */
    @Override
    public String extractKey(OAuth2Authentication authentication)
    {
        Map<String, String> valoresParametrosAutenticacion;
        OAuth2Request peticionOAuth2;
        MessageDigest messageDigest;

        valoresParametrosAutenticacion = new HashMap<>();
        peticionOAuth2 = authentication.getOAuth2Request();

        valoresParametrosAutenticacion.put("username", authentication.getName());
        valoresParametrosAutenticacion.put("client_id", peticionOAuth2.getClientId());
        valoresParametrosAutenticacion.put("scope", OAuth2Utils.formatParameterList(peticionOAuth2.getScope()));
        valoresParametrosAutenticacion.put("uuid", UUID.randomUUID().toString());

        try
        {
            messageDigest = MessageDigest.getInstance("MD5");
            byte[] bytes = messageDigest.digest(valoresParametrosAutenticacion.toString().getBytes("UTF-8"));
            return String.format("%032x", new BigInteger(1, bytes));
        } catch (NoSuchAlgorithmException e)
        {
            throw new IllegalStateException("Erro en GeneradorClavesAutenticacion (NoSuchAlgorithmException): " + e.getMessage());
        } catch (UnsupportedEncodingException e)
        {
            throw new IllegalStateException("Error en GeneradorClavesAutenticacion (UnsupportedEncodingException): " + e.getMessage());
        }
    }
}
