/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz
 * ismael.ortiz@sesaemm.org.mx
 */
public class OAuth2ExceptionsSerializer extends StdSerializer<OAuth2ExceptionSerialization>
{

    /**
     *
     */
    public OAuth2ExceptionsSerializer()
    {
        super(OAuth2ExceptionSerialization.class);
    }

    /**
     * @param excepcionOAuth2
     * @param jsonGenerator
     * @param serializerProvider
     * @throws IOException
     */
    @Override
    public void serialize(OAuth2ExceptionSerialization excepcionOAuth2, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
    {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("code");
        jsonGenerator.writeString(String.valueOf(excepcionOAuth2.getHttpErrorCode()));
        jsonGenerator.writeFieldName("message");
        jsonGenerator.writeString(excepcionOAuth2.getMessage());
        jsonGenerator.writeEndObject();
    }
}
