package tol.sesaemm.ing.cvlg.controlador;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.s2spic.FILTRO_SPIC_WS;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.SPICROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@RestController
public class ControladorServicioRest
{

    /**
     * Mensaje principale del web service
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String webapp()
    {
        return "Servicio Web de conexión SESAEMM - SESNA";
    }

    /**
     * Obtiene en formato Json estructura peticion Post (atributos de paginacion
     * y resultados de consulta) de servidores publicos que intervienen en
     * contrataciones
     *
     * @param Bcuerpo Estructura Post
     * @return Cadena de estructura Post en formato Json
     * @throws java.lang.Exception
     */
    @RequestMapping(value = "/v1/spic", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String obtenerEstructuraPostFormatoJsonServidoresPublicosIntervienenContrataciones(@RequestBody(required = false) String Bcuerpo) throws Exception
    {
        LogicaDeNegocio logicaNegocio;
        FILTRO_SPIC_WS estructuraPost;
        SPICROOT spic;                
        SPIC camposPublicos;          
        Document documentoDeError;    

        logicaNegocio = new LogicaDeNegocioV01();

        try
        {

            Bcuerpo = new String(Bcuerpo.getBytes("ISO-8859-1"), "UTF-8");
            estructuraPost = new Gson().fromJson(Bcuerpo, FILTRO_SPIC_WS.class);
            estructuraPost = logicaNegocio.obtenerValoresPorDefaultFiltroSpic(estructuraPost);

            if (logicaNegocio.esValidoRFC(estructuraPost.getQuery().getRfcSolicitante()) || estructuraPost.getQuery().getRfcSolicitante() == null || estructuraPost.getQuery().getRfcSolicitante().isEmpty())
            {

                spic = logicaNegocio.obtenerServidoresPublicosIntervienenContratacionesPost(estructuraPost);
                camposPublicos = logicaNegocio.obtenerEstatusDePrivacidadCamposServidoresPublicosIntervienenContrataciones();

                logicaNegocio.guardarBitacoraDeEventos(estructuraPost, spic.getPagination(), "Correcta");

                return logicaNegocio.generarJsonServidoresPublicosIntervienenContrataciones(spic, camposPublicos, estructuraPost.getQuery().getRfcSolicitante());
            }
            else
            {

                Document error = new Document();
                error.append("code", "spic_p02");
                error.append("message", "Error el RFC no es válido. ");

                logicaNegocio.guardarBitacoraDeEventos(estructuraPost, null, "El RFC no es válido");

                return error.toJson();
            }
        }
        catch (Exception ex)
        {

            documentoDeError = new Document();
            documentoDeError.append("code", "spic_p01");
            documentoDeError.append("message", "Error al obtener listado de servidores públicos que intervienen en contrataciones: " + ex.toString());

            estructuraPost = logicaNegocio.obtenerValoresPorDefaultFiltroSpic(new FILTRO_SPIC_WS());
            logicaNegocio.guardarBitacoraDeEventos(estructuraPost, null, "Error al obtener listado de servidores públicos que intervienen en contrataciones: " + ex.toString());

            return documentoDeError.toJson();
        }
    }

    /**
     * Obtiene dependecias en formato Json de servidores publicos que
     * intervienen en contrataciones
     *
     * @return Cadena de dependencias en formato Json
     */
    @RequestMapping(value = "/v1/spic/dependencias", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String obtenerDependenciasServidoresPublicosIntervienenContrataciones()
    {
        LogicaDeNegocio LN;                 
        Document documentoDeError;          

        try
        {
            LN = new LogicaDeNegocioV01();
            return LN.generarJsonDependenciasServidoresPublicosIntervienenContrataciones(LN.obtenerDependenciasServidoresPublicosIntervienenContrataciones());
        }
        catch (Exception ex)
        {

            documentoDeError = new Document();
            documentoDeError.append("code", "Ssancionados_p01");
            documentoDeError.append("message", "Error al obtener listado de Dependencias: " + ex.toString());
            return documentoDeError.toJson();
        }
    }

    @RequestMapping(value = "/403", produces = "application/json;charset=UTF-8")
    public String error403()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "403");
        documentoDeError.append("message", "Forbidden");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/404", produces = "application/json;charset=UTF-8")
    public String error404()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "404");
        documentoDeError.append("message", "Not Found");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/405", produces = "application/json;charset=UTF-8")
    public String error405()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "405");
        documentoDeError.append("message", "Method Not Allowed");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/500", produces = "application/json;charset=UTF-8")
    public String error500()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "500");
        documentoDeError.append("message", "Internal Server Error");

        return documentoDeError.toJson();
    }
}